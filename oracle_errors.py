# -*- coding: utf-8 -*-
"""
Created on Thu Dec 20 09:53:32 2018

@author: Priya
"""

#%% Importing libraries

from bs4 import BeautifulSoup
import urllib
import pandas as pd

#%% Opening particular website

url = "https://docs.oracle.com/cd/B28359_01/server.111/b28278/e900.htm#ORA-00910"    
html = urllib.request.urlopen(url).read()    

#%% Get html code 
soup = BeautifulSoup(html , "lxml")    # lxml - parser   
print(soup.prettify())

#%% Get everything inside div class
soup.div
#%% Getting all div having class msgentry

msgentries = soup.find_all('div',class_ = "msgentry")
msgentries

#%% Getting Title , Action , Cause for one msgentry

one = msgentries[0]
Title = one.dl.dt.text
print("\n",Title)

Cause = one.find("div",class_ = "msgexplan")
print("\n",Cause.text)

Action = one.find("div",class_ = "msgaction")
print("\n",Action.text)

#%% Getting Title , Action , Cause for all msgentry

for i in range(len(msgentries)):
    msg = msgentries[i]
    Title = msg.dl.dt.text
    print("\n",Title)
    
    Cause = msg.find("div",class_ = "msgexplan")
    print(Cause.text)
    
    Action = msg.find("div",class_ = "msgaction")
    print(Action.text)
    print("\n")
#%% Creating empty lists

idx = []
title = []
cause = []
action = []

#%% Appending title , action , cause to lists
for i in range(len(msgentries)):
    msg = msgentries[i]
    idx.append(i)
    
    Title = msg.dl.dt.text
    title.append(Title)
    
    Cause = msg.find("div",class_ = "msgexplan")
    cause.append(Cause.text)
    
    Action = msg.find("div",class_ = "msgaction")
    action.append(Action.text)

#%% Storing saved lists in dataframe

errors = pd.DataFrame()
errors['index'] = idx
errors['title'] = title
errors['cause'] = cause
errors['action'] = action
    

#%% Export to CSV

errors.to_csv("errors_373.csv", header = True, index = False)
