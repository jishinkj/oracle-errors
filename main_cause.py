import pandas as pd
import prompts

#%% Load the dataframe 

df = pd.read_csv("errors_373.csv")

df['error_code'] = df['title'].apply(lambda x: x.split(':')[0])
df['error_desc'] = df['title'].map(lambda x : x.split(':')[1])

list_of_errors = list(df['error_code'])
#%%

while True:
    lang = str(input("Which language do you want to converse in?"))
    if lang.lower() == prompts.dutch_lang_strings:
        lang_flag = 1
    else:
        lang_flag = 0
        
    # multi-language input 
    input_str = str(input("\n" + prompts.first_message)) 
    if input_str == 'q':
        print("\n"+ prompts._exit)
        break
    elif input_str in list_of_errors:
        idx = list_of_errors.index(input_str)
        error_code = df.loc[idx,'error_code']
        error_desc = df.loc[idx,'error_desc']
        
        print("\nThe error code is {} and error description is{}".format(error_code,error_desc))  
        
        # cause chunk
        cause_inp = str(input("\n"+prompts.cause))
        if cause_inp.lower() in prompts.positive_words:
            print("\nThe cause of the error is", df.loc[idx,'cause'].split('Cause: ')[1])
        elif cause_inp.lower() in prompts.negative_words:
            print("\nOkay, noted")
#            ask_action = False
        
        # action chunk
        action_inp = str(input("\n"+prompts.action))
        if action_inp in prompts.positive_words:
            print("\nThe action to be taken is", df.loc[idx,'action'].split('Action: ')[1])
            print("\nCool! Have fun! ")
        elif action_inp in prompts.negative_words:
            print("\nOkay! Hope your problem was resolved! ")
        
    else:
        print("\n"+ prompts.again)
#%%
        
        
        
        
## Importance-wise
        
# 1. Regex matching for error codes
# 2. Multiple prompts for the same intent
# 3. 

        
        
        
        
        
        
        
        
        
        
        
