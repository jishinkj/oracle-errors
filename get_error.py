# error code processing

import re

#%% Different types of inputs to the error codes

#e1 = "ORA-00910" # covered
#e2 = "ora-00910" # covered
#e3 = "ora 00910" # covered
#e4 = "ora00910"  # covered
#e5 = "00910"
#e6 = "910"
#e7 = "or-00910"

#all_errors = e1 + e2 + e3 + e4 + e5 + e6 + e7
#%%

class GetError:
    
    def __init__(self, inp_error):
        self.inp_error = inp_error.lower()
        
    
    def _clean(self):
        '''
        Function to identify the character part
        and numeric part of the entire error code.
        
        '''
        err_text = re.findall(r'\D{3}', self.inp_error)
        err_digits = re.findall(r'\d{5}', self.inp_error)
        return err_text, err_digits
    
    def find_error(self):
        e_text, e_digits = self._clean()
        error = e_text[0].upper() + '-' + e_digits[0]
        # can just put the above line in the previous method
        # and avoid writing this method. But find_error method 
        # will later contain the logic to identify the error
        # if the inputted error isn't of length 8 or 9
        return error
    
#%%
while True:
    inp_error = str(input("\nEnter the Error: "))
    try:
        if inp_error != 'q':
            err_obj = GetError(inp_error)
            cleaned_error = err_obj.find_error()
            print("\nIdentified Error is", cleaned_error)
        else:
            break
    except:
        print("\nOops! Please enter the error code again!")
        continue
#%% match 3 characters and 5 numbers

#err_text = re.findall(r'\D{3}', e1)
#err_digits = re.findall(r'\d{5}', e1)


#error = err_text[0].upper() + '-' + err_digits[0]

#%%

#inp_error = str(input("Enter the Error: "))
#if len(inp_error) == 9:
#    error = re.findall(r'\w{3}-\d{5}', inp_error)
#    
#    if len(error) == 0:
#        error = re.findall(r'\w{3} \d{5}', inp_error)
    
