import pandas as pd

#%%

df = pd.read_csv("errors_373.csv")


#%%

df['error_code'] = df['title'].apply(lambda x: x.split(':')[0])
df['error_desc'] = df['title'].map(lambda x : x.split(':')[1])


#%%

while True:
    
    input_str = str(input("\nEnter the error code : "))
    if input_str == 'q':
        print("\nThank you! See you soon! :)")
        break
    else:
        try:  
            for idx, err_code in enumerate(df['error_code']):
                
                if input_str == err_code:
                    err_desc = df.loc[idx, 'error_desc']
                    err_action = df.loc[idx, 'action']
                    err_cause = df.loc[idx, 'cause']
                    
            print("\nThe error is {} and error description is {}.".format(err_code, err_desc))
            print("\nCause of this error is :", err_cause.split('Cause: ')[1])
            print("\nAction to be taken are :", err_action.split('Action: ')[1])
            print("\nInput 'q' to exit")
                    
            err_desc = ""
            err_cause = ""
            err_action = ""

        except :
            print("\nSorry, that error code wasn't a valid one. Please type the error code again!")
