# First message
first_message = ["What error are you facing? ", "Enter the error code : "]

# Cause prompts
cause = ["Would you like to know the cause? ", " "]

cause = {
        'Would you want to know the cause?': 0,
         'Zou je de oorzaak willen weten?': 1
        }


# Action prompts
action = "Would you like to know the action? "

# Prompt-again prompts
again = "Sorry, please enter the error code again!"

# Exit prompts
_exit = ["Thank you! See you soon! :)", "Thank you for using this service!"]

# =============================================================================
# =============================================================================

positive_words = ['yeah', 'yes', 'yup', 'ya']

negative_words = ['nah','no','nope']

# =============================================================================
# =============================================================================
# Language

dutch_lang_strings = ["dutch", "netherlands"]

# =============================================================================